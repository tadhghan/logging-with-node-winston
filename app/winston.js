/*
Default NPM error levels
{
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
}
*/

const winston = require('winston')
const moment  = require('moment')
console.log(moment().format())

winston.level = 'info'; //anything beneath info will not be printed
winston.error('Lack of Frop error', {timestamp: moment().format(), pid: process.pid, location: 'tadhgs files'})
winston.warn('Lack of Frop warn', {timestamp: moment().format(), pid: process.pid})
winston.info('Lack of Frop info', {timestamp: moment().format(), pid: process.pid})
winston.verbose('Lack of Frop verbose', {timestamp: moment().format(), pid: process.pid})
winston.debug('Lack of Frop debug', {timestamp: moment().format(), pid: process.pid})
winston.silly('Lack of Frop silly', {timestamp: moment().format(), pid: process.pid})

/*
console.log(process.env.LOG_LEVEL)
winston.level = 'debug';
winston.log('info', 'Hello log files!', {
  someKey: 'some-value'
})
*/

//winston.add(winston.transports.File, { filename: 'somefile.log' })
//winston.remove(winston.transports.Console)
//winston.info('Hello world')
//winston.debug('Debugging info')

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.File)({
      name: 'info-file',
      filename: 'filelog-info.log',
      level: 'info'
    }),
    new (winston.transports.File)({
      name: 'error-file',
      filename: 'filelog-error.log',
      level: 'error'
    })
  ]
});
logger.info('Sending info');
logger.error('Does not compute');*/

//testing what happens when cloning into a repo that has been changed.
