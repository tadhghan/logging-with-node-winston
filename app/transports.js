const winston = require('winston')
const moment  = require('moment')
//const console = new winston.transports.Console();

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (winston.transports.File)({ filename: 'somefile.log' })
  ]
});

const gorilla  = new (winston.Logger)({
  level: 'info',
  transports: [
    new winston.transports.Console({timestamp: () => { return moment().format('YYYY-MM-DD hh:mm:ss')}, colorize: true, showLevel: true})
  ]
})

gorilla.error('An error occured and was logged successfully')
gorilla.silly('Wubalubadubdub')
